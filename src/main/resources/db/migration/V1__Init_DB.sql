-- create sequence contact_seq start 1 increment 1;
create table contact (id int auto_increment primary key,
            name varchar(255) not null, phone varchar(255) not null);
