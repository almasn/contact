package com.almasn.contact.model.dto;

import com.almasn.contact.model.base.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ContactDTO extends BaseDTO {

    @NotBlank
    private String name;

    @NotBlank
    private String phone;
}
