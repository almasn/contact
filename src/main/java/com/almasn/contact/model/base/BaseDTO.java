package com.almasn.contact.model.base;

public abstract class BaseDTO {

    protected Integer id;

    public BaseDTO() {
    }

    public BaseDTO(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
