package com.almasn.contact.model.base;

import java.io.Serializable;

public interface BaseModel extends Serializable {

    void setId(Integer id);

    Integer getId();
}
