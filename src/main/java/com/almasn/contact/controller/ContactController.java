package com.almasn.contact.controller;

import com.almasn.contact.model.dto.ContactDTO;
import com.almasn.contact.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Objects;

@Controller
public class ContactController {

    private final ContactService service;

    @Autowired
    public ContactController(ContactService service) {
        this.service = service;
    }

    @RequestMapping("/index")
    public String showContactList(ContactDTO contact, Model model) {

        if (Objects.nonNull(contact) && Objects.nonNull(contact.getName())) {
            model.addAttribute("contact", contact);
        } else {
            model.addAttribute("contact", new ContactDTO());
        }

        model.addAttribute("contactList", service.searchContact(contact));
        return "index";
    }

    @GetMapping("/addcontact")
    public String showAddForm(Model model) {
        model.addAttribute("contact", new ContactDTO());
        return "add-contact";
    }

    @PostMapping("/addcontact")
    public String addContact(@Valid ContactDTO contact, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-contact";
        }

        service.create(contact);
        return "redirect:/index";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        ContactDTO contact = service.find(id).orElseThrow(() -> new IllegalArgumentException("Invalid contact Id:" + id));
        model.addAttribute("contact", contact);

        return "update-contact";
    }

    @PostMapping("/update/{id}")
    public String updateContact(@PathVariable("id") Integer id, @Valid ContactDTO contact, BindingResult result, Model model) {
        if (result.hasErrors()) {
            contact.setId(id);
            return "update-contact";
        }

        service.create(contact);

        return "redirect:/index";
    }

    @GetMapping("/delete/{id}")
    public String deleteContact(@PathVariable("id") Integer id, Model model) {
        ContactDTO contact = service.find(id).orElseThrow(() -> new IllegalArgumentException("Invalid contact Id:" + id));
        service.delete(id);

        return "redirect:/index";
    }
}
