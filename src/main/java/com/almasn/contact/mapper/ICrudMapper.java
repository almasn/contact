package com.almasn.contact.mapper;

import com.almasn.contact.model.base.BaseDTO;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface ICrudMapper<DTO extends BaseDTO, Entity> {

    Entity toEntity(DTO dto);

    DTO toDTO(Entity entity);

}
