package com.almasn.contact.mapper;

import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Service;

@Service
public interface IOrikaMapper {

    void addMapping(MapperFactory mapperFactory);

}
