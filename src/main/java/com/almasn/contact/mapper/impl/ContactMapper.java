package com.almasn.contact.mapper.impl;

import com.almasn.contact.mapper.IOrikaMapper;
import com.almasn.contact.model.dto.ContactDTO;
import com.almasn.contact.model.entity.Contact;
import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public class ContactMapper implements IOrikaMapper {
    @Override
    public void addMapping(MapperFactory mapperFactory) {
        mapperFactory.classMap(Contact.class, ContactDTO.class)
                .byDefault()
                .register();
    }
}
