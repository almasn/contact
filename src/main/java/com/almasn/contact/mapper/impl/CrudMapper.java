package com.almasn.contact.mapper.impl;

import com.almasn.contact.mapper.ICrudMapper;
import com.almasn.contact.mapper.IOrikaMapper;
import com.almasn.contact.model.base.BaseDTO;
import com.almasn.contact.model.base.BaseModel;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;


@Component
public class CrudMapper<DTO extends BaseDTO, Entity extends BaseModel> implements ICrudMapper<DTO, Entity> {
    protected MapperFacade mapper = null;

    @Autowired
    private List<IOrikaMapper> mappers;

    @PostConstruct
    public void init() {
        if (mapper == null) {
            final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
            mappers.forEach(mappingSupplier -> mappingSupplier.addMapping(mapperFactory));
            this.mapper = mapperFactory.getMapperFacade();
        }
    }

    @Override
    public Entity toEntity(DTO dto) {
        return (Entity) mapper.map(dto, BaseModel.class);
    }

    @Override
    public DTO toDTO(Entity entity) {
        return (DTO) mapper.map(entity, BaseDTO.class);
    }
}
