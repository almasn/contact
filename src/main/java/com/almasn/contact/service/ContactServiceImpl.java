package com.almasn.contact.service;

import com.almasn.contact.mapper.ICrudMapper;
import com.almasn.contact.model.dto.ContactDTO;
import com.almasn.contact.model.entity.Contact;
import com.almasn.contact.repository.ContactRepository;
import com.almasn.contact.service.base.CrudFacade;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ContactServiceImpl extends CrudFacade<ContactDTO, Contact> implements ContactService {

    private final ContactRepository repo;
    ICrudMapper<ContactDTO, Contact> mapper;

    public ContactServiceImpl(ContactRepository dao, ICrudMapper<ContactDTO, Contact> mapper) {
        super(dao, mapper);
        this.repo = dao;
        this.mapper = mapper;
    }

    @Override
    public List<ContactDTO> searchContact(ContactDTO contact) {
        return repo.findAll().stream()
                .filter(el -> Objects.isNull(contact.getName()) || el.getName().toLowerCase().contains(contact.getName().toLowerCase()))
                .filter(el -> Objects.isNull(contact.getPhone()) || el.getPhone().toLowerCase().contains(contact.getPhone().toLowerCase()))
                .map(el -> mapper.toDTO(el)).collect(Collectors.toList());
    }
}
