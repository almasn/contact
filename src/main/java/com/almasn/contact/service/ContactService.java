package com.almasn.contact.service;

import com.almasn.contact.model.dto.ContactDTO;
import com.almasn.contact.service.base.ICrudFacade;

import java.util.List;

public interface ContactService extends ICrudFacade<ContactDTO> {

    public List<ContactDTO> searchContact(ContactDTO contact);

}
