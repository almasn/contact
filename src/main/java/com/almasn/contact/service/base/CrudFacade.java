package com.almasn.contact.service.base;

import com.almasn.contact.mapper.ICrudMapper;
import com.almasn.contact.model.base.BaseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CrudFacade<DTO extends BaseDTO, Entity> implements ICrudFacade<DTO> {

    protected final JpaRepository<Entity, Integer> dao;
    protected final ICrudMapper<DTO, Entity> mapper;

    public CrudFacade(JpaRepository<Entity, Integer> dao, ICrudMapper<DTO, Entity> mapper) {
        this.dao = dao;
        this.mapper = mapper;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DTO> find(Integer id) {
        Optional<Entity> entity = dao.findById(id);
        return entity.map(mapper::toDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DTO> findAll() {
        return dao.findAll().stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public Page<DTO> findAll(Pageable pageable) {
        return dao.findAll(pageable).map(mapper::toDTO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public DTO create(DTO dto) {
        Entity entity = mapper.toEntity(dto);
        return mapper.toDTO(dao.save(entity));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createBulk(List<DTO> dtos) {
        List<Entity> entities = dtos.stream().map(mapper::toEntity).collect(Collectors.toList());
        dao.saveAll(entities);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(Integer id, DTO dto) {
        dto.setId(id);
        Entity entity = mapper.toEntity(dto);
        dao.save(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        dao.deleteById(id);
    }
}
