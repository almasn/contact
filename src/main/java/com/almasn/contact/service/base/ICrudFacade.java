package com.almasn.contact.service.base;

import com.almasn.contact.model.base.BaseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface ICrudFacade<DTO extends BaseDTO> {

    Optional<DTO> find(Integer id);

    List<DTO> findAll();

    Page<DTO> findAll(Pageable pageable);

    DTO create(DTO dto);

    void createBulk(List<DTO> dtos);

    void edit(Integer id, DTO dto);

    void delete(Integer id);

}
